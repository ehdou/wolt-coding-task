package services.models

import org.scalatestplus.play.PlaySpec

class OpeningHoursTest extends PlaySpec {
  "WeekDays" should {
    "return the correct previous day" in {
      WeekDays.previousDay(WeekDays.Monday) mustEqual WeekDays.Sunday
      WeekDays.previousDay(WeekDays.Tuesday) mustEqual WeekDays.Monday
      WeekDays.previousDay(WeekDays.Wednesday) mustEqual WeekDays.Tuesday
      WeekDays.previousDay(WeekDays.Thursday) mustEqual WeekDays.Wednesday
      WeekDays.previousDay(WeekDays.Friday) mustEqual WeekDays.Thursday
      WeekDays.previousDay(WeekDays.Saturday) mustEqual WeekDays.Friday
      WeekDays.previousDay(WeekDays.Sunday) mustEqual WeekDays.Saturday
    }
  }
}
