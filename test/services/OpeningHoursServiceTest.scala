package services

import org.scalatestplus.play.PlaySpec
import services.models.{OpeningHours, OpeningHoursException, OpeningPeriod, OpeningState, WeekDays}

class OpeningHoursServiceTest extends PlaySpec {
  private val service = new OpeningHoursServiceImpl()

  "OpeningHoursService" should {
    "return the transformed opening hours" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq.empty,
        WeekDays.Tuesday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 64800)),
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Thursday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 64800)),
        WeekDays.Friday -> Seq(OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Saturday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Sunday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 43200), OpeningHours(OpeningState.Close, 75600))
      )
      val expected = Map(
        WeekDays.Monday -> Seq.empty,
        WeekDays.Tuesday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Thursday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Friday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Saturday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Sunday -> Seq(OpeningPeriod(Some(43200), Some(75600)))
      )

      service.transform(openingHours) mustEqual expected
    }

    "transform the opening hours even if the request is not ordered" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq.empty,
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Sunday -> Seq( OpeningHours(OpeningState.Close, 75600), OpeningHours(OpeningState.Open, 43200), OpeningHours(OpeningState.Close, 3600)),
        WeekDays.Tuesday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 64800)),
        WeekDays.Saturday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 3600)),
        WeekDays.Thursday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 64800)),
        WeekDays.Friday -> Seq(OpeningHours(OpeningState.Open, 36000))
      )
      val expected = Map(
        WeekDays.Monday -> Seq.empty,
        WeekDays.Tuesday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Thursday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Friday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Saturday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Sunday -> Seq(OpeningPeriod(Some(43200), Some(75600)))
      )

      service.transform(openingHours) mustEqual expected
    }

    "shift time correctly for every day of the week" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Tuesday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Wednesday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Thursday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Friday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Saturday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Sunday -> Seq(OpeningHours(OpeningState.Close, 3600), OpeningHours(OpeningState.Open, 36000))
      )
      val expected = Map(
        WeekDays.Monday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Tuesday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Wednesday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Thursday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Friday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Saturday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Sunday -> Seq(OpeningPeriod(Some(36000), Some(3600)))
      )

      service.transform(openingHours) mustEqual expected
    }

    "throw an exception if the opening hours are not valid" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Open, 40000), OpeningHours(OpeningState.Close, 75600))
      )

      a[OpeningHoursException] must be thrownBy {
        val result = service.transform(openingHours)
        result mustEqual null
      }
    }

    "throw an exception if the closing hours are not valid" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 36000), OpeningHours(OpeningState.Close, 40000), OpeningHours(OpeningState.Close, 75600))
      )

      a[OpeningHoursException] must be thrownBy {
        val result = service.transform(openingHours)
        result mustEqual null
      }
    }

    "throw an exception if the restaurant is open for more than 2 days in a row" in {
      val openingHours = Map(
        WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 36000)),
        WeekDays.Wednesday -> Seq(OpeningHours(OpeningState.Close, 40000))
      )

      a[OpeningHoursException] must be thrownBy {
        val result = service.transform(openingHours)
        result mustEqual null
      }
    }
  }
}
