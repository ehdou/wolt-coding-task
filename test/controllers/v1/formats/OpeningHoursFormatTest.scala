package controllers.v1.formats

import controllers.v1.models.{OpeningHoursApi, OpeningStateApi, WeekDaysApi}
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.{JsSuccess, Json}

class OpeningHoursFormatTest extends PlaySpec {
  "OpeningHoursFormat" should {
    "read a request properly" in {
      val request = Json.parse("""{
        "monday" : [],
        "tuesday" : [
          {
            "type" : "open",
            "value" : 36000
          },
          {
            "type" : "close",
            "value" : 64800
          }
        ],
        "wednesday" : [],
        "thursday" : [
          {
            "type" : "open",
            "value" : 36000
          },
          {
            "type" : "close",
            "value" : 64800
          }
        ],
        "friday" : [
          {
            "type" : "open",
            "value" : 36000
          }
        ],
        "saturday" : [
          {
            "type" : "close",
            "value" : 3600
          },
          {
            "type" : "open",
            "value" : 36000
          }
        ],
        "sunday" : [
          {
            "type" : "close",
            "value" : 3600
          },
          {
            "type" : "open",
            "value" : 43200
          },
          {
            "type" : "close",
            "value" : 75600
          }
        ]
        }""")
      val expected = Map(
        WeekDaysApi.Monday -> Seq.empty,
        WeekDaysApi.Tuesday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 36000), OpeningHoursApi(OpeningStateApi.Close, 64800)),
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 36000), OpeningHoursApi(OpeningStateApi.Close, 64800)),
        WeekDaysApi.Friday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 36000)),
        WeekDaysApi.Saturday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 3600), OpeningHoursApi(OpeningStateApi.Open, 36000)),
        WeekDaysApi.Sunday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 3600), OpeningHoursApi(OpeningStateApi.Open, 43200), OpeningHoursApi(OpeningStateApi.Close, 75600))
      )
      OpeningHoursFormat.openingHoursRequestFormat.reads(request) mustEqual JsSuccess(expected)
    }
  }
}
