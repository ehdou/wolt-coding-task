package controllers.v1.validations

import controllers.v1.models.{OpeningHoursApi, OpeningStateApi, WeekDaysApi}
import org.scalatestplus.play.PlaySpec

class OpeningHoursRequestValidatorTest extends PlaySpec {
  private val validator = new OpeningHoursRequestValidatorImpl()

  "OpeningHoursRequestValidator" should {
    "return Right if the input is valid" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600), OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Tuesday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Right(())
    }

    "return an error if the value is negative" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, -1), OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Tuesday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Left("Opening hours should be between 0 and 86399")
    }

    "return an error if the value is too big" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 86400)),
        WeekDaysApi.Tuesday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Left("Opening hours should be between 0 and 86399")
    }

    "accept 00:00:00 as an input" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 0), OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Tuesday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Right(())
    }

    "accept 23:59:59 as an input" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 86399)),
        WeekDaysApi.Tuesday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Right(())
    }

    "return an error if opening hours are duplicated for the same day" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3000), OpeningHoursApi(OpeningStateApi.Close, 3000)),
        WeekDaysApi.Tuesday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Left("Some days contain duplicated opening hours")
    }

    "return an error if some week days are missing from the request" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Left("tuesday, friday missing from the request")
    }

    "return an error if the number of opening entries and closing entries are not equal" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600)),
        WeekDaysApi.Tuesday -> Seq.empty,
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq.empty,
        WeekDaysApi.Friday -> Seq.empty,
        WeekDaysApi.Saturday -> Seq.empty,
        WeekDaysApi.Sunday -> Seq.empty,
      )
      validator.validate(request) mustEqual Left("The amount of opening entries and closing entries should be equal")
    }
  }
}
