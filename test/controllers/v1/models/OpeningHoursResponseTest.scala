package controllers.v1.models

import org.scalatestplus.play.PlaySpec
import services.models.{OpeningPeriod, WeekDays}

class OpeningHoursResponseTest extends PlaySpec {
  "OpeningHoursResponse" should {
    "convert response to string" in {
      val response = Map(
        WeekDays.Monday -> Seq.empty,
        WeekDays.Tuesday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Thursday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Friday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Saturday -> Seq(OpeningPeriod(Some(36000), Some(3600))),
        WeekDays.Sunday -> Seq(OpeningPeriod(Some(43200), Some(75600)))
      )

      OpeningHoursResponse(response) mustEqual(
        "Monday: Closed\n" +
        "Tuesday: 10:00 AM - 6:00 PM\n" +
        "Wednesday: Closed\n" +
        "Thursday: 10:00 AM - 6:00 PM\n" +
        "Friday: 10:00 AM - 1:00 AM\n" +
        "Saturday: 10:00 AM - 1:00 AM\n" +
        "Sunday: 12:00 AM - 9:00 PM")
    }

    "order the response correctly" in {
      val response = Map(
        WeekDays.Tuesday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Thursday -> Seq(OpeningPeriod(Some(36000), Some(64800))),
        WeekDays.Monday -> Seq.empty,
      )

      OpeningHoursResponse(response) mustEqual(
        "Monday: Closed\n" +
        "Tuesday: 10:00 AM - 6:00 PM\n" +
        "Thursday: 10:00 AM - 6:00 PM")
    }

    "support multiple periods" in {
      val response = Map(
        WeekDays.Monday -> Seq(OpeningPeriod(Some(3600), Some(36000)), OpeningPeriod(Some(39600), Some(64800))),
      )

      OpeningHoursResponse(response) mustEqual "Monday: 1:00 AM - 10:00 AM, 11:00 AM - 6:00 PM"
    }

    "order periods correctly" in {
      val response = Map(
        WeekDays.Monday -> Seq(OpeningPeriod(Some(39600), Some(64800)), OpeningPeriod(Some(3600), Some(36000))),
      )

      OpeningHoursResponse(response) mustEqual "Monday: 1:00 AM - 10:00 AM, 11:00 AM - 6:00 PM"
    }

    "support minutes" in {
      val response = Map(
        WeekDays.Monday -> Seq(OpeningPeriod(Some(4200), Some(37800))),
      )

      OpeningHoursResponse(response) mustEqual "Monday: 1:10 AM - 10:30 AM"
    }
  }
}
