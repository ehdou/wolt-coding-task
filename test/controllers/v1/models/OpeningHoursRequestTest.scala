package controllers.v1.models

import org.scalatestplus.play.PlaySpec
import services.models.{OpeningHours, OpeningState, WeekDays}

class OpeningHoursRequestTest extends PlaySpec {
  "OpeningHoursRequest" should {
    "be converted into the internal representation" in {
      val request = Map(
        WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 1200)),
        WeekDaysApi.Tuesday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Wednesday -> Seq.empty,
        WeekDaysApi.Thursday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600), OpeningHoursApi(OpeningStateApi.Close, 36000)),
        WeekDaysApi.Friday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600), OpeningHoursApi(OpeningStateApi.Close, 36000), OpeningHoursApi(OpeningStateApi.Open, 40000)),
        WeekDaysApi.Saturday -> Seq(OpeningHoursApi(OpeningStateApi.Close, 3600)),
        WeekDaysApi.Sunday -> Seq.empty
      )
      val expected = Map(
        WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 1200)),
        WeekDays.Tuesday -> Seq(OpeningHours(OpeningState.Close, 36000)),
        WeekDays.Wednesday -> Seq.empty,
        WeekDays.Thursday -> Seq(OpeningHours(OpeningState.Open, 3600), OpeningHours(OpeningState.Close, 36000)),
        WeekDays.Friday -> Seq(OpeningHours(OpeningState.Open, 3600), OpeningHours(OpeningState.Close, 36000), OpeningHours(OpeningState.Open, 40000)),
        WeekDays.Saturday -> Seq(OpeningHours(OpeningState.Close, 3600)),
        WeekDays.Sunday -> Seq.empty
      )
      OpeningHoursRequest.toInternal(request) mustEqual expected
    }
  }
}
