package controllers.v1


import akka.stream.Materializer
import controllers.v1.models.OpeningHoursRequest.OpeningHoursRequest
import controllers.v1.models.{OpeningHoursApi, OpeningStateApi, WeekDaysApi}
import controllers.v1.validations.OpeningHoursRequestValidator
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.OpeningHoursService
import services.models.{OpeningHours, OpeningHoursException, OpeningPeriod, OpeningState, WeekDays}

class OpeningHoursControllerTest extends PlaySpec with GuiceOneAppPerSuite with MockitoSugar {

  implicit lazy val materializer: Materializer = app.materializer
  private val openingHoursService = mock[OpeningHoursService]
  private val openingHoursValidator = mock[OpeningHoursRequestValidator]
  private val component = app.injector.instanceOf(classOf[ControllerComponents])
  private val controller = new OpeningHoursController(component, openingHoursService, openingHoursValidator)

  private def mockOpeningHoursServiceTransform(input: Map[WeekDays.Value, Seq[OpeningHours]], output: Map[WeekDays.Value, Seq[OpeningPeriod]]) = {
    when(openingHoursService.transform(input)) thenReturn output
  }

  private def mockOpeningHoursValidator(input: OpeningHoursRequest, output: Either[String, Unit]) = {
    when(openingHoursValidator.validate(input)) thenReturn output
  }

  "OpeningHoursController" should {
    "return the transformed opening hours" in {
      mockOpeningHoursValidator(Map(WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600), OpeningHoursApi(OpeningStateApi.Close, 36000))), Right(()))
      mockOpeningHoursServiceTransform(
        Map(WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 3600), OpeningHours(OpeningState.Close, 36000))),
        Map(WeekDays.Monday -> Seq(OpeningPeriod(Some(3600), Some(36000))))
      )

      val request = FakeRequest(POST, "/v1/transform").withJsonBody(Json.parse("""{"monday":[{"type": "open", "value": 3600}, {"type": "close", "value": 36000}]}"""))
      val result = call(controller.transform, request)

      status(result) mustEqual OK
      contentAsString(result) mustEqual """Monday: 1:00 AM - 10:00 AM"""
    }

    "return an error if the input is not following the correct format" in {
      val request = FakeRequest(POST, "/v1/transform").withJsonBody(Json.parse("""{"wrong": "format"}"""))
      val result = call(controller.transform, request)

      status(result) mustEqual BAD_REQUEST
      contentAsString(result) mustEqual "(,List(JsonValidationError(List(Failed to read OpeningHoursRequest),WrappedArray())))"
    }

    "return an error if the input is invalid" in {
      val error = "Error!!!"
      mockOpeningHoursValidator(Map(WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, -1))), Left(error))

      val request = FakeRequest(POST, "/v1/transform").withJsonBody(Json.parse("""{"monday":[{"type": "open", "value": -1}]}"""))
      val result = call(controller.transform, request)

      status(result) mustEqual BAD_REQUEST
      contentAsString(result) mustEqual error
    }

    "return an error if the service throws an exception" in {
      val error = "Error!!!"
      mockOpeningHoursValidator(Map(WeekDaysApi.Monday -> Seq(OpeningHoursApi(OpeningStateApi.Open, 3600), OpeningHoursApi(OpeningStateApi.Close, 36000))), Right(()))
      when(openingHoursService.transform(Map(WeekDays.Monday -> Seq(OpeningHours(OpeningState.Open, 3600), OpeningHours(OpeningState.Close, 36000))))) thenThrow OpeningHoursException(error)

      val request = FakeRequest(POST, "/v1/transform").withJsonBody(Json.parse("""{"monday":[{"type": "open", "value": 3600}, {"type": "close", "value": 36000}]}"""))
      val result = call(controller.transform, request)

      status(result) mustEqual BAD_REQUEST
      contentAsString(result) mustEqual error
    }
  }
}

