import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.Status
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._

class OpeningHoursRouteTest extends PlaySpec with GuiceOneAppPerSuite {

  "OpeningHoursRoute" should {
    "return the transformed opening hours" in {
      val body = Json.parse("""{
        "monday" : [],
        "tuesday" : [
          {
            "type" : "open",
            "value" : 36000
          },
          {
            "type" : "close",
            "value" : 64800
          }
        ],
        "wednesday" : [],
        "thursday" : [
          {
            "type" : "open",
            "value" : 36000
          },
          {
            "type" : "close",
            "value" : 64800
          }
        ],
        "friday" : [
          {
            "type" : "open",
            "value" : 36000
          }
        ],
        "saturday" : [
          {
            "type" : "close",
            "value" : 3600
          },
          {
            "type" : "open",
            "value" : 36000
          }
        ],
        "sunday" : [
          {
            "type" : "close",
            "value" : 3600
          },
          {
            "type" : "open",
            "value" : 43200
          },
          {
            "type" : "close",
            "value" : 75600
          }
        ]
        }"""
      )
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.OK
      contentAsString(response) must equal (
        "Monday: Closed\n" +
        "Tuesday: 10:00 AM - 6:00 PM\n" +
        "Wednesday: Closed\n" +
        "Thursday: 10:00 AM - 6:00 PM\n" +
        "Friday: 10:00 AM - 1:00 AM\n" +
        "Saturday: 10:00 AM - 1:00 AM\n" +
        "Sunday: 12:00 AM - 9:00 PM"
      )
    }

    "accept empty inputs" in {
      val body = Json.parse("""{
        "monday" : [],
        "tuesday" : [],
        "wednesday" : [],
        "thursday" : [],
        "friday" : [],
        "saturday" : [],
        "sunday" : []
        }"""
      )
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.OK
      contentAsString(response) must equal (
        "Monday: Closed\n" +
        "Tuesday: Closed\n" +
        "Wednesday: Closed\n" +
        "Thursday: Closed\n" +
        "Friday: Closed\n" +
        "Saturday: Closed\n" +
        "Sunday: Closed"
      )
    }

    "return an error if the JSON format is not valid" in {
      val body = Json.parse("""{"wrong" : "format"}"""
      )
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "(,List(JsonValidationError(List(Failed to read OpeningHoursRequest),WrappedArray())))"
      )
    }

    "return an error if the opening times are invalid" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": -1}],
          "tuesday":[],
          "wednesday":[],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "Opening hours should be between 0 and 86399"
      )
    }

    "return an error if some days are missing from the request" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": -1}],
          "tuesday":[],
          "wednesday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "thursday, friday, saturday, sunday missing from the request"
      )
    }

    "return an error if the request contains duplicated opening hours" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 36000}, {"type": "open", "value": 36000}],
          "tuesday":[],
          "wednesday":[],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "Some days contain duplicated opening hours"
      )
    }

    "return an error if multiple 'open' states are following each others" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 2000}, {"type": "open", "value": 4000}],
          "tuesday":[{"type": "close", "value": 4000}],
          "wednesday":[{"type": "close", "value": 4000}],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "The restaurant cannot be open unless it is closed first"
      )
    }

    "return an error if multiple 'open' states are following each others on consecutive days" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 2000}],
          "tuesday":[{"type": "open", "value": 4000}],
          "wednesday":[{"type": "close", "value": 4000}],
          "thursday":[{"type": "close", "value": 4000}],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "The restaurant cannot be closed unless it is open first"
      )
    }

    "return an error if different amount of open and closed entries are present" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 2000}],
          "tuesday":[{"type": "open", "value": 4000}],
          "wednesday":[{"type": "close", "value": 4000}],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "The amount of opening entries and closing entries should be equal"
      )
    }

    "return an error if multiple 'close' states are following each others" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 2000}, {"type": "close", "value": 4000}, {"type": "close", "value": 6000}],
          "tuesday":[{"type": "open", "value": 2000}],
          "wednesday":[],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "The restaurant cannot be closed unless it is open first"
      )
    }

    "return an error if multiple 'close' states are following each others on consecutive days" in {
      val body = Json.parse(
        """{
          "monday":[{"type": "open", "value": 2000}, {"type": "close", "value": 4000}],
          "tuesday":[{"type": "close", "value": 6000}],
          "wednesday":[{"type": "open", "value": 2000}],
          "thursday":[],
          "friday":[],
          "saturday":[],
          "sunday":[]
        }""".stripMargin)
      val response = route(app, FakeRequest(POST, "/v1/transform").withJsonBody(body)).get

      status(response) mustBe Status.BAD_REQUEST
      contentAsString(response) must equal (
        "The restaurant cannot be closed unless it is open first"
      )
    }
  }
}

