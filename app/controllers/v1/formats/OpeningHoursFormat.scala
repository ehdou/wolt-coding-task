package controllers.v1.formats

import controllers.v1.models.OpeningHoursRequest.OpeningHoursRequest
import controllers.v1.models.{OpeningHoursApi, OpeningStateApi, WeekDaysApi}
import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json._

object OpeningHoursFormat {
  implicit val openingStateFormat = Json.formatEnum(OpeningStateApi)
  implicit val weekDaysFormat = Json.formatEnum(WeekDaysApi)
  implicit val openingHoursFormat = Json.format[OpeningHoursApi]

  implicit val openingHoursRequestReads: Reads[OpeningHoursRequest] =
    (jsValue: JsValue) => {
      jsValue.asOpt[Map[String, Seq[OpeningHoursApi]]] match {
        case Some(valueAsMap) =>
          val parsedWeekDays = valueAsMap.map { case (k, v) =>  WeekDaysApi.values.find(_.toString == k) -> v }
          if (parsedWeekDays.exists(_._1.isEmpty)) {
            JsError("Failed to read OpeningHoursRequest, invalid week days")
          } else {
            JsSuccess(parsedWeekDays.map { case (k, v) => k.get -> v})
          }
        case None => JsError("Failed to read OpeningHoursRequest")
      }
    }

  implicit val openingHoursRequestWrites: Writes[OpeningHoursRequest] =
    (request: OpeningHoursRequest) => Json.obj(request.map { case (weekDay, openingHours) =>
      val ret: (String, JsValueWrapper) = weekDay.toString -> openingHours.map(openingHoursFormat.writes)
      ret
    }.toSeq: _*)

  implicit val openingHoursRequestFormat: Format[OpeningHoursRequest] = Format(openingHoursRequestReads, openingHoursRequestWrites)
}
