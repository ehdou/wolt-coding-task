package controllers.v1

import controllers.v1.formats.OpeningHoursFormat._
import controllers.v1.models.OpeningHoursRequest.OpeningHoursRequest
import controllers.v1.models.{OpeningHoursRequest, OpeningHoursResponse}
import controllers.v1.validations.OpeningHoursRequestValidator
import javax.inject.{Inject, Singleton}
import play.api.libs.json.JsValue
import play.api.mvc.{AbstractController, Action, ControllerComponents, Request}
import services.OpeningHoursService
import services.models.OpeningHoursException

@Singleton
class OpeningHoursController @Inject()(cc: ControllerComponents,
                                       openingHoursService: OpeningHoursService,
                                       openingHoursRequestValidator: OpeningHoursRequestValidator) extends AbstractController(cc) {
  def transform: Action[JsValue] = Action(parse.tolerantJson) { request: Request[JsValue] =>
    request.body.validate[OpeningHoursRequest].fold(
      errors => BadRequest(errors.mkString),
      body => openingHoursRequestValidator.validate(body) match {
        case Left(error) => BadRequest(error)
        case Right(_) =>
          try {
            Ok(OpeningHoursResponse(openingHoursService.transform(OpeningHoursRequest.toInternal(body))))
          } catch {
            case error: OpeningHoursException => BadRequest(error.getMessage)
          }
      }
    )
  }
}
