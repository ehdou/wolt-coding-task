package controllers.v1.validations

import controllers.v1.models.OpeningHoursRequest.OpeningHoursRequest
import controllers.v1.models.{OpeningStateApi, WeekDaysApi}

trait  OpeningHoursRequestValidator {
  def validate(request: OpeningHoursRequest): Either[String, Unit]
}

class OpeningHoursRequestValidatorImpl extends OpeningHoursRequestValidator {
  private val endOfDay = (24 * 60 * 60) - 1

  override def validate(request: OpeningHoursRequest): Either[String, Unit] = {
    for {
      _ <- validateWeekDays(request)
      _ <- validateOpeningHours(request)
      _ <- validateUniqueOpeningHours(request)
      _ <- validateOpeningHoursStatesParity(request)
    } yield {
      Right(())
    }
  }

  private def validateOpeningHours(request: OpeningHoursRequest): Either[String, Unit] = {
    if (request.values.forall(_.forall(openingHours => openingHours.value >= 0 && openingHours.value <= endOfDay))) {
      Right(())
    } else {
      Left(s"Opening hours should be between 0 and $endOfDay")
    }
  }

  private def validateWeekDays(request: OpeningHoursRequest): Either[String, Unit] = {
    WeekDaysApi.values.diff(request.keySet).toSeq match {
      case Nil => Right(())
      case days => Left(days.mkString(", ") + " missing from the request")
    }
  }

  private def validateUniqueOpeningHours(request: OpeningHoursRequest): Either[String, Unit] = {
    if (request.values.forall(_.groupBy(_.value).forall(_._2.length == 1))) {
      Right(())
    } else {
      Left("Some days contain duplicated opening hours")
    }
  }

  private def validateOpeningHoursStatesParity(request: OpeningHoursRequest): Either[String, Unit] = {
    val openingHoursStates = request.values.flatMap(_.map(_.`type`))
    val (closingEntries, openingEntries) = openingHoursStates.partition(_ == OpeningStateApi.Close)
    if (closingEntries.size == openingEntries.size) {
      Right(())
    } else {
      Left("The amount of opening entries and closing entries should be equal")
    }
  }
}
