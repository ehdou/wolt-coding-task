package controllers.v1.models

import services.models.{OpeningPeriod, WeekDays}

object OpeningHoursResponse {
  def apply(response: Map[WeekDays.Value, Seq[OpeningPeriod]]): String = {
   response.toSeq.sortBy(_._1).map(weekDayToString).mkString("\n")
  }

  private def weekDayToString(weekDay: (WeekDays.Value, Seq[OpeningPeriod])): String = {
    val day = weekDay._1
    val periods = weekDay._2.sortBy(_.open).map(periodToString) match {
      case Nil => "Closed"
      case list => list.mkString(", ")
    }

    s"$day: $periods"
  }

  private def periodToString(period: OpeningPeriod): String = {
    val openingTime = period.open.map(convertTime).getOrElse("")
    val closingTime = period.close.map(convertTime).getOrElse("")
    s"$openingTime - $closingTime"
  }

  private def convertTime(value: Int): String = {
    val minutes = (value / 60) % 60
    val hours = value / 3600
    if (hours > 12) {
      val updatedHours = hours - 12
      f"$updatedHours:$minutes%02d PM"
    } else {
      f"$hours:$minutes%02d AM"
    }
  }
}
