package controllers.v1.models

import services.models.{OpeningHours, OpeningState, WeekDays}

case class OpeningHoursApi(
  `type`: OpeningStateApi.Value,
  value: Int
) {
  def toInternal: OpeningHours = {
    OpeningHours(OpeningStateApi.toInternal(`type`), value)
  }
}

object WeekDaysApi extends Enumeration {
  val Monday: Value = Value("monday")
  val Tuesday: Value = Value("tuesday")
  val Wednesday: Value = Value("wednesday")
  val Thursday: Value = Value("thursday")
  val Friday: Value = Value("friday")
  val Saturday: Value = Value("saturday")
  val Sunday: Value = Value("sunday")

  def toInternal(day: WeekDaysApi.Value): WeekDays.Value = {
    day match {
      case Monday => WeekDays.Monday
      case Tuesday => WeekDays.Tuesday
      case Wednesday => WeekDays.Wednesday
      case Thursday => WeekDays.Thursday
      case Friday => WeekDays.Friday
      case Saturday => WeekDays.Saturday
      case Sunday => WeekDays.Sunday
    }
  }
}

object OpeningStateApi extends Enumeration {
  val Open: Value = Value("open")
  val Close: Value = Value("close")

  def toInternal(state: OpeningStateApi.Value): OpeningState.Value = {
    state match {
      case Open => OpeningState.Open
      case Close => OpeningState.Close
    }
  }
}

object OpeningHoursRequest {
  type OpeningHoursRequest = Map[WeekDaysApi.Value, Seq[OpeningHoursApi]]

  def toInternal(request: OpeningHoursRequest): Map[WeekDays.Value, Seq[OpeningHours]] = {
    request.map { case (weekDay, openingHours) => (WeekDaysApi.toInternal(weekDay), openingHours.map(_.toInternal))}
  }
}

