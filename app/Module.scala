import java.time.Clock

import com.google.inject.AbstractModule
import controllers.v1.validations.{OpeningHoursRequestValidator, OpeningHoursRequestValidatorImpl}
import services.{OpeningHoursService, OpeningHoursServiceImpl}

class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)

    bind(classOf[OpeningHoursService]).to(classOf[OpeningHoursServiceImpl])
    bind(classOf[OpeningHoursRequestValidator]).to(classOf[OpeningHoursRequestValidatorImpl])
  }

}
