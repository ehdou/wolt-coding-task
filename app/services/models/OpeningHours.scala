package services.models

case class OpeningHours(
  `type`: OpeningState.Value,
  value: Int
)

object WeekDays extends Enumeration {
  val Monday: Value = Value(1)
  val Tuesday: Value = Value(2)
  val Wednesday: Value = Value(3)
  val Thursday: Value = Value(4)
  val Friday: Value = Value(5)
  val Saturday: Value = Value(6)
  val Sunday: Value = Value(7)

  def previousDay(day: WeekDays.Value): WeekDays.Value = {
    day match {
      case Monday => WeekDays.Sunday
      case Tuesday => WeekDays.Monday
      case Wednesday => WeekDays.Tuesday
      case Thursday => WeekDays.Wednesday
      case Friday => WeekDays.Thursday
      case Saturday => WeekDays.Friday
      case Sunday => WeekDays.Saturday
    }
  }
}

object OpeningState extends Enumeration {
  val Open: Value = Value("open")
  val Close: Value = Value("close")
}

case class OpeningPeriod(
  open: Option[Int],
  close: Option[Int]
)
