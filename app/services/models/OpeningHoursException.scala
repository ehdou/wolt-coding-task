package services.models

final case class OpeningHoursException(
  private val message: String = "",
  private val cause: Throwable = None.orNull
) extends Exception(message, cause)
