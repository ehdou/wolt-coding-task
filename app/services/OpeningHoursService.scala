package services

import services.models.{OpeningHours, OpeningHoursException, OpeningPeriod, OpeningState, WeekDays}

trait OpeningHoursService {
  @throws(classOf[OpeningHoursException])
  def transform(openingHours: Map[WeekDays.Value, Seq[OpeningHours]]): Map[WeekDays.Value, Seq[OpeningPeriod]]
}

class OpeningHoursServiceImpl extends OpeningHoursService {
  override def transform(openingHours: Map[WeekDays.Value, Seq[OpeningHours]]): Map[WeekDays.Value, Seq[OpeningPeriod]] = {
    val sortedOpeningHours = openingHours.toSeq.sortBy(_._1).map { case (day, hours) => (day, hours.sortBy(_.value)) }
    val shiftedOpeningHours = shiftOpeningHours(sortedOpeningHours)
    shiftedOpeningHours.mapValues(parseOpeningPeriods)
  }

  /**
    * Shifts closing time to the previous day if the restaurant is opened through the night
    *
    * Transforms:
    *   - Monday: Open 10 AM
    *   - Tuesday: Close 1 AM, Open 10 AM, Close 4 PM
    * Into;
    *   - Monday: Open 10 AM, Close 1 AM
    *   - Tuesday: Open 10 AM, Close 4 PM
    */
  private def shiftOpeningHours(openingHours: Seq[(WeekDays.Value, Seq[OpeningHours])]): Map[WeekDays.Value, Seq[OpeningHours]] = {
    openingHours.foldLeft[Map[WeekDays.Value, Seq[OpeningHours]]](Map.empty)((shiftedOpeningHours, dailyOpeningHours) => {
      if (dailyOpeningHours._2.headOption.exists(_.`type` == OpeningState.Close)) {
        val previousWeekDay = WeekDays.previousDay(dailyOpeningHours._1)
        val updatedDay = previousWeekDay -> (shiftedOpeningHours.getOrElse(previousWeekDay, Seq.empty) :+ dailyOpeningHours._2.head)
        val currentDay = dailyOpeningHours._1 -> dailyOpeningHours._2.drop(1)
        shiftedOpeningHours ++ insertDayToMap(shiftedOpeningHours, currentDay) + updatedDay
      } else {
        insertDayToMap(shiftedOpeningHours, dailyOpeningHours)
      }
    })
  }

  private def insertDayToMap(map: Map[WeekDays.Value, Seq[OpeningHours]],
                             dailyOpeningHours: (WeekDays.Value, Seq[OpeningHours])): Map[WeekDays.Value, Seq[OpeningHours]] = {
    if (map.contains(dailyOpeningHours._1)) {
      val updatedDay = dailyOpeningHours._1 -> (dailyOpeningHours._2 ++ map(dailyOpeningHours._1))
      map + updatedDay
    } else {
      map + dailyOpeningHours
    }
  }

  /**
    * Transforms opening times into opening periods
    *
    * Transforms:
    *   - Monday: Open 10 AM, Close 1 AM
    *   - Tuesday: Open 10 AM, Close 4 PM
    * Into;
    *   - Monday: 10 AM -> 1 AM
    *   - Tuesday: 10 AM -> 4 PM
    */
  private def parseOpeningPeriods(openingHours: Seq[OpeningHours]): Seq[OpeningPeriod] = {
    openingHours.foldLeft[Seq[OpeningPeriod]](Seq.empty)((periods, openingHours) => {
      openingHours.`type` match {
        case OpeningState.Open => periods.lastOption match {
          case Some(period) if period.close.isEmpty => throw new OpeningHoursException("The restaurant cannot be open unless it is closed first")
          case _ => periods :+ OpeningPeriod(Some(openingHours.value), None)
        }
        case OpeningState.Close => periods.lastOption match {
          case Some(period) if period.close.isEmpty => periods.dropRight(1) :+ period.copy(close = Some(openingHours.value))
          case _ => throw new OpeningHoursException("The restaurant cannot be closed unless it is open first")
        }
      }
    })
  }
}
