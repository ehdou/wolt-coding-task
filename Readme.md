# Wolt coding task

## Part 1

### Running the application
The application can be started with `sbt run`. It will start a server listening on port 9000.
The requests should be POSTed to `http://localhost:9000/v1/transform`.

Example: `curl -d '{...}' -X POST http://localhost:9000/v1/transform`

### Testing the application
Tests can be run with `sbt test`.

## Part 2

Instead of specifying the state of the restaurant (`open` or `close`) for each entry, the JSON could contain a list of
periods of opening. Each period would contain a start and end time. In order to support opening hours spanning across
multiple days, the time should contain a day and a value as UNIX time (from 0 to 86399).
```
[
    {
        "open": {"day": "monday", "time": 36000},
        "close": {"day": "tuesday", "time": 3600}
    },
    {
        ...
    }
]
```
Alternatively, `day` and `time` could be combined into a single value as UNIX time (from 0 to 604799)
```
[
    {
        "open": 36000,
        "close": 90000
    },
    {
        ...
    }
]
```
This solution would probably reduce the size of the payload but would make the request more difficult to write by hand.
One aspect to keep in mind when handling this request is the possibility of conflicting periods, but that should easily
be solved with a validation rule.
